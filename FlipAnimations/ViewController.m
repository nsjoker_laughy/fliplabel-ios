//
//  ViewController.m
//  FlipAnimations
//
//  Created by NSJoker on 25/11/15.
//  Copyright © 2015 Arkham Asylum. All rights reserved.
//


/*
 This is the sample implemntation of Fliplabel.
 */
#import "ViewController.h"
#import "FlipLabel.h"

#define ANIMATE_SPEED 2.0

@interface ViewController ()

@property (nonatomic) int count;

@property (nonatomic,strong) FlipLabel *topToBottomFlipLabel;
@property (nonatomic,strong) FlipLabel *bottomToTopFlipLabel;
@property (nonatomic,strong) FlipLabel *rightToLeftFlipLabel;
@property (nonatomic,strong) FlipLabel *leftToRightFlipLabel;
@property (nonatomic,strong) FlipLabel *multiDirectionalFlipLabel;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.count = 100;
    
    [self createviews];
}

- (void)createviews {
    
    /*
     To change label styles use lblTop and lblBottom in FlipLabel.
     lblTop is what is going to come next and lblBottom is what is visible before animation.
     */
    
    self.topToBottomFlipLabel = [[FlipLabel alloc] initWithText:[NSString stringWithFormat:@"%d",self.count] Frame:CGRectMake((CGRectGetWidth(self.view.frame)/2)-20, 100, 40, 40) FlipStyle:FlipStyleTopToBottom];
    self.topToBottomFlipLabel.animateSpeed = ANIMATE_SPEED;
    self.topToBottomFlipLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.topToBottomFlipLabel];
    
    self.bottomToTopFlipLabel = [[FlipLabel alloc] initWithText:[NSString stringWithFormat:@"%d",self.count] Frame:CGRectMake((CGRectGetWidth(self.view.frame)/2)-20, 200, 40, 40) FlipStyle:FlipStyleBottomToTop];
    self.bottomToTopFlipLabel.animateSpeed = ANIMATE_SPEED;
    self.bottomToTopFlipLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.bottomToTopFlipLabel];
    
    self.rightToLeftFlipLabel = [[FlipLabel alloc] initWithText:[NSString stringWithFormat:@"%d",self.count] Frame:CGRectMake((CGRectGetWidth(self.view.frame)/2)-20, 300, 40, 40) FlipStyle:FlipStyleRightToLeft];
    self.rightToLeftFlipLabel.animateSpeed = ANIMATE_SPEED;
    self.rightToLeftFlipLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.rightToLeftFlipLabel];
    
    self.leftToRightFlipLabel = [[FlipLabel alloc] initWithText:[NSString stringWithFormat:@"%d",self.count] Frame:CGRectMake((CGRectGetWidth(self.view.frame)/2)-20, 400, 40, 40) FlipStyle:FlipStyleLeftToRight];
    self.leftToRightFlipLabel.animateSpeed = ANIMATE_SPEED;
    self.leftToRightFlipLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.leftToRightFlipLabel];
    
    self.multiDirectionalFlipLabel = [[FlipLabel alloc] initWithText:[NSString stringWithFormat:@"%d",self.count] Frame:CGRectMake((CGRectGetWidth(self.view.frame)/2)-20, 500, 40, 40) FlipStyle:FlipStyleLeftToRight];
    self.multiDirectionalFlipLabel.animateSpeed = ANIMATE_SPEED;
    self.multiDirectionalFlipLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.multiDirectionalFlipLabel];
    
    [self performSelector:@selector(tapp) withObject:nil afterDelay:ANIMATE_SPEED];
}

- (void)tapp {
    
    if (self.count < 990) {
        self.count += 10;
    } else {
        self.count = 100;
    }
    
    [self.topToBottomFlipLabel prepareForNextFlipWithText:[NSString stringWithFormat:@"%d",self.count]];
    [self.topToBottomFlipLabel flipOnce];
    
    [self.bottomToTopFlipLabel prepareForNextFlipWithText:[NSString stringWithFormat:@"%d",self.count]];
    [self.bottomToTopFlipLabel flipOnce];
    
    [self.rightToLeftFlipLabel prepareForNextFlipWithText:[NSString stringWithFormat:@"%d",self.count]];
    [self.rightToLeftFlipLabel flipOnce];
    
    [self.leftToRightFlipLabel prepareForNextFlipWithText:[NSString stringWithFormat:@"%d",self.count]];
    [self.leftToRightFlipLabel flipOnce];
    
    if (self.multiDirectionalFlipLabel.flipStyle == FlipStyleTopToBottom) {
        self.multiDirectionalFlipLabel.flipStyle = FlipStyleLeftToRight;
    } else if (self.multiDirectionalFlipLabel.flipStyle == FlipStyleLeftToRight) {
        self.multiDirectionalFlipLabel.flipStyle = FlipStyleRightToLeft;
    } else if (self.multiDirectionalFlipLabel.flipStyle == FlipStyleRightToLeft) {
        self.multiDirectionalFlipLabel.flipStyle = FlipStyleBottomToTop;
    } else if (self.multiDirectionalFlipLabel.flipStyle == FlipStyleBottomToTop) {
        self.multiDirectionalFlipLabel.flipStyle = FlipStyleTopToBottom;
    }
    
    [self.multiDirectionalFlipLabel prepareForNextFlipWithText:[NSString stringWithFormat:@"%d",self.count]];
    [self.multiDirectionalFlipLabel flipOnce];
    
    
    [self performSelector:@selector(tapp) withObject:nil afterDelay:ANIMATE_SPEED];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
