//
//  AppDelegate.h
//  FlipAnimations
//
//  Created by NSJoker on 25/11/15.
//  Copyright © 2015 Arkham Asylum. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

