//
//  FlipLabel.m
//  FlipAnimations
//
//  Created by NSJoker on 25/11/15.
//  Copyright © 2015 Arkham Asylum. All rights reserved.
//

#import "FlipLabel.h"

@interface FlipLabel ()

@property (nonatomic, strong) NSString *initialText;

@end

@implementation FlipLabel

- (id)initWithText:(NSString *)text Frame:(CGRect)frame FlipStyle:(FlipStyle)flipStyle{
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        self.initialText = @"";
        self.animateSpeed = 1.0;
        self.flipStyle = FlipStyleNone;
        
        if (text.length > 0) {
            self.initialText = text;
        }
        
        self.flipStyle = flipStyle;
        
        [self createViews];
    }
    
    return self;
}

- (void)createViews {
    
    self.backgroundColor = [UIColor clearColor];
    
    self.lblTop = [[UILabel alloc] initWithFrame:self.bounds];
    self.lblTop.text = self.initialText;
    self.lblTop.textAlignment = NSTextAlignmentCenter;
    self.lblTop.textColor = [UIColor blackColor];
    self.lblTop.font = [UIFont boldSystemFontOfSize:20];
    [self addSubview:self.lblTop];
    
    self.lblBottom = [[UILabel alloc] initWithFrame:self.bounds];
    self.lblBottom.text = self.initialText;
    self.lblBottom.textAlignment = NSTextAlignmentCenter;
    self.lblBottom.textColor = [UIColor blackColor];
    self.lblBottom.font = [UIFont boldSystemFontOfSize:20];
    [self addSubview:self.lblBottom];
    
    
    switch (self.flipStyle) {
        case FlipStyleNone:
        case FlipStyleTopToBottom: {
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 1.0, 0.0, 0.0);
            self.lblTop.center = CGPointMake(self.frame.size.width/2, 0);
        }
            break;
        case FlipStyleBottomToTop: {
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, -1.0, 0.0, 0.0);
            self.lblTop.center = CGPointMake(self.frame.size.width/2, self.frame.size.height);
        }
            break;
        case FlipStyleRightToLeft: {
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 1.0, 0.0);
            self.lblTop.center = CGPointMake(0, self.frame.size.height/2);
        }
            break;
        case FlipStyleLeftToRight: {
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, -1.0, 0.0);
            self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame), self.frame.size.height/2);
        }
            break;
            
        default:
            break;
    }
}

- (void)prepareForNextFlipWithText:(NSString *)text {
    
    self.lblBottom.text = self.lblTop.text;
    self.lblTop.text = text;
    
    self.lblTop.frame = self.bounds;
    self.lblBottom.frame = self.bounds;
    
    switch (self.flipStyle) {
        
        case FlipStyleTopToBottom: {
            self.lblTop.center = CGPointMake(self.frame.size.width/2, 0);
            
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 1.0, 0.0, 0.0);
            self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
        }
            break;
        case FlipStyleBottomToTop: {
            self.lblTop.center = CGPointMake(self.frame.size.width/2, self.frame.size.height);
            
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, -1.0, 0.0, 0.0);
            self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
        }
            break;
        case FlipStyleRightToLeft: {
            self.lblTop.center = CGPointMake(0, self.frame.size.height/2);
            
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 1.0, 0.0);
            self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
        }
            break;
        case FlipStyleLeftToRight: {
            self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame), self.frame.size.height/2);
            
            self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, -1.0, 0.0);
            self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
        }
            break;
            
        default: {
        }
            break;
    }
    
    self.lblTop.alpha = 0.0;
    self.lblBottom.alpha = 1.0;
}

- (void)reset {
    
    [self prepareForNextFlipWithText:self.initialText];
}

- (void)flipOnce {
    
    switch (self.flipStyle) {
        
        case FlipStyleTopToBottom: {
            
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
                                 self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
                                 self.lblTop.alpha = 1.0;
                                 
                             }
                             completion:nil];
            
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, -1.0, 0.0, 0.0);
                                 self.lblBottom.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame));
                                 self.lblBottom.alpha = 0.0;
                             }
                             completion:nil];
            
        }
            break;
        case FlipStyleBottomToTop: {
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
                                 self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
                                 self.lblTop.alpha = 1.0;
                                 
                             }
                             completion:nil];
            
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, -1.0, 0.0, 0.0);
                                 self.lblBottom.center = CGPointMake(CGRectGetWidth(self.frame)/2, 0);
                                 self.lblBottom.alpha = 0.0;
                             }
                             completion:nil];
        }
            break;
        case FlipStyleRightToLeft: {
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
                                 self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
                                 self.lblTop.alpha = 1.0;
                                 
                             }
                             completion:nil];
            
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, -1.0, 0.0);
                                 self.lblBottom.center = CGPointMake(CGRectGetWidth(self.frame), CGRectGetHeight(self.frame)/2);
                                 self.lblBottom.alpha = 0.0;
                             }
                             completion:nil];
        }
            break;
        case FlipStyleLeftToRight: {
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblTop.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 0.0);
                                 self.lblTop.center = CGPointMake(CGRectGetWidth(self.frame)/2, CGRectGetHeight(self.frame)/2);
                                 self.lblTop.alpha = 1.0;
                                 
                             }
                             completion:nil];
            
            [UIView animateWithDuration:self.animateSpeed
                                  delay:0
                                options:UIViewAnimationOptionCurveLinear
                             animations:^{self.lblBottom.layer.transform = CATransform3DMakeRotation(M_PI_2, 0.0, 1.0, 0.0);
                                 self.lblBottom.center = CGPointMake(0, CGRectGetHeight(self.frame)/2);
                                 self.lblBottom.alpha = 0.0;
                             }
                             completion:nil];
        }
            break;
            
        default:
            break;
    }
}

@end
