//
//  FlipLabel.h
//  FlipAnimations
//
//  Created by NSJoker on 25/11/15.
//  Copyright © 2015 Arkham Asylum. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    FlipStyleTopToBottom,
    FlipStyleBottomToTop,
    FlipStyleRightToLeft,
    FlipStyleLeftToRight,
    FlipStyleNone
}FlipStyle;

@interface FlipLabel : UIView

/*Adjust the animation speed of flips, if no value specified it is set to 1.*/
@property (nonatomic) float animateSpeed;

/*Set the flip direction of the label, check the enum FlipStyle, if no value specified then set to FlipStyleNone.*/
@property (nonatomic) FlipStyle flipStyle;

/*The label which is visible after flip animation. Set text for this label direclty at your own risk.*/
@property (nonatomic, strong) UILabel *lblTop;

/*The label which is visible after flip animation. Set text for this label direclty at your own risk.*/
@property (nonatomic, strong) UILabel *lblBottom;

/*Init your fliplabel object using this instance.*/
- (id)initWithText:(NSString*)text Frame:(CGRect)frame FlipStyle:(FlipStyle)flipStyle;

/*Change text and prepare for flip*/
- (void)prepareForNextFlipWithText:(NSString *)text;

/*Flip once*/
- (void)flipOnce;

/*Resets the Fliplabel text to initial text provided*/
- (void)reset;

@end
